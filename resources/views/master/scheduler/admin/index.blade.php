@extends('layouts.lay')

@section('content')

                <div class="col-sm-12">
        @if(!empty(\Session::get('success')) > 0)
                    <div class="alert alert-success">     
                        <strong>Sukses!</strong> {!! \Session::get('success') !!}             
                        </div>
                    @endif
                <script>
                    window.setTimeout(function() {
                    $.noConflict();
                    $(".alert").fadeTo(500, 0).slideUp(500, function(){
                        $(this).remove(); 
                    });
                    }, 5000);
                </script>
                <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
                <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
                </div>
				
          <div class="content-header-left col-12 mb-2 mt-1">
                    <div class="breadcrumbs-top">
                        <h5 class="content-header-title float-left pr-1 mb-0">Dashboard</h5>
                        <div class="breadcrumb-wrapper d-none d-sm-block">
                            <ol class="breadcrumb p-0 mb-0 pl-1">
                                <li class="breadcrumb-item"><a href="<?php echo url("/"); ?>"><i class="bx bx-home-alt"></i></a>
                                </li>
                                <li class="breadcrumb-item active">Profile
                                </li>
                                <!-- <li class="breadcrumb-item active">
                                </li> -->
                            </ol>
                        </div>
                    </div>
                </div>
                    <div class="row">
                        <!-- Task Card Starts -->
                        <!-- <div class="col-lg-12">
                            <div class="row">
                                <div class="col-12">
                                <div class="card card-primary card-outline card-outline-tabs"> -->
                        
                        <div class="col-md-6 col-12">
                        <div class="card card-primary card-outline card-outline-tabs">
                            <div class="card-body">
                                <div class="card-header">
                                    <img class="img-thumbnail rounded mx-auto d-block" src="{{url('/app-assets/images/pages/Logo2.png')}}" alt="branding logo" style="width:150px;height:150px;">
                                </div>
                            
                            </div>
                        </div>
                        </div>




                        <div class="col-md-6 col-12">
                        <div class="card card-primary card-outline card-outline-tabs">
                        <div class="card-header">
                            <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="custom-tabs-one-home-tab" data-toggle="pill" href="#custom-tabs-one-home" role="tab" aria-controls="custom-tabs-one-home" aria-selected="true">Update Profile</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="custom-tabs-one-profile-tab" data-toggle="pill" href="#custom-tabs-one-profile" role="tab" aria-controls="custom-tabs-one-profile" aria-selected="false">Update Password</a>
                            </li>
                            </ul>
                        </div>

                            <div class="card-body">
                            <div class="tab-content" id="custom-tabs-one-tabContent">
                            <div class="tab-pane fade show active" id="custom-tabs-one-home" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
                            <div class="card">
                                <!-- /.card-header -->
                                <div class="form-group">
                                    <label>Nama Pegawai</label>
                                    <input type="text" class="form-control" id="name" name="name" value="" required>
                                </div>

                                <div class="form-group">
                                    <label>NIP</label>
                                    <input type="text" class="form-control" id="name" name="name" value="" required>
                                </div>

                                <div class="form-group">
                                    <label>Pangkat</label>
                                    <input type="text" class="form-control" id="name" name="name" value="" required>
                                </div>

                                <div class="form-group">
                                    <label>Avatar</label>
                                    <input type="file" class="form-control" id="name" name="name" value="" required>
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-sm float-right">Update Profile</button>       
                                </div>

                            </div>
                            </div>

                            <div class="tab-pane fade" id="custom-tabs-one-profile" role="tabpanel" aria-labelledby="custom-tabs-one-profile-tab">
                            <div class="card">
                                <!-- /.card-header -->
                                <div class="form-group">
                                    <label>Password Lama</label>
                                    <input type="text" class="form-control" id="name" name="name" value="" required>
                                </div>

                                <div class="form-group">
                                    <label>Password Baru</label>
                                    <input type="text" class="form-control" id="name" name="name" value="" required>
                                </div>

                                <div class="form-group">
                                    <label>Konfirmasi Password</label>
                                    <input type="text" class="form-control" id="name" name="name" value="" required>
                                </div>

                                    <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-sm float-right">Update Password</button>       
                                    </div>
                                <!-- /.form-group -->
                            </div>
                            </div>
                            </div>
                            </div>
                        </div>
                        </div>
                        </div>
                        </div>
                <!-- /.card -->
                </div>


                                <!-- </div>
                            </div>
                        </div>
                    </div> -->
@endsection